'''
Created on Oct 21, 2014

@author: xiaolin
'''

from mainsetting import *
from modelfunction import *
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import pickle


# truth----the whole domain
truth_grid_array = SimFunc(X_truth_grid_array, Y_truth_grid_array,K_truth)

# plot three domains in one figure
fig = plt.figure()
ax = fig.gca(projection='3d')
surf = ax.plot_surface(X_truth_grid_array, Y_truth_grid_array, np.exp(truth_grid_array), rstride=1, cstride=1, cmap=cm.coolwarm,
        linewidth=0, antialiased=False)
ax.zaxis.set_major_locator(LinearLocator(3))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

fig.colorbar(surf, shrink=0.5, aspect=3)
ax.set_xlabel('x')
ax.set_xlim(1, 5)
ax.set_xticks([1, 3, 5])
ax.set_ylabel('y')
ax.set_ylim(-2, 2)
ax.set_yticks([-2, 0, 2])
ax.set_zlabel('z')

#ax.view_init(elev=elevation_angle, azim=azimuthal_angle)
#plt.savefig('destination_path.eps', format='eps', dpi=300)
plt.savefig('Figures/3dTruth.eps',format='eps', dpi=1200)

# initial samples
p_pf_prop = np.tile(p_bounds_array[0][0], (1, num_samples)) + np.tile(p_bounds_array[0][1], (1, num_samples))*np.random.rand(1, num_samples)

p_pf_initial = p_pf_prop[0]

with open('p_pf_initial.pickle', 'w') as f:
    pickle.dump(p_pf_initial, f)

plt.figure()
plt.pcolor(X_truth_grid_array, Y_truth_grid_array, np.exp(truth_grid_array), cmap='rainbow', vmin=np.exp(truth_grid_array).min(), vmax=np.exp(truth_grid_array).max())
plt.title('truth')
# set the limits of the plot to the limits of the data
plt.axis([X_truth_grid_array.min(), X_truth_grid_array.max(), Y_truth_grid_array.min(), Y_truth_grid_array.max()])
plt.colorbar()
plt.xticks([1,3,5])
plt.yticks([-2,0,2])
plt.ylabel('y')
plt.xlabel('x')
plt.savefig('Figures/2dTruth.eps',format='eps', dpi=1200)

