'''
Created on Oct 21, 2014

@author: xiaolin
'''
import numpy as np

# prior distribution for parameters, but will not be used since it will be defined directly in the pystan block
p_bounds_array = np.array([[0.0000001, 5.0]])

# number of parameters
num_para = 1

# true parameters
K_truth = 1.0

# measurement noise
err_perc = 0.1
meas_std = err_perc
R = meas_std**2

#Number of trials
num_trials = 1

# Number of stages
num_stages = 10

# Number of samples
num_samples = 2000

# define domains:1) domain to show the truth only for plots 2) domain to do inverse problem 3) domain to do prediction

# truth domain is used to generate truth figure with high resolution
domain = np.array([[1.,5.],[-2.,2.]])
x_truth_span_array = np.linspace(1., 5., 100)  
y_truth_span_array = np.linspace(-2., 2., 100)    
[X_truth_grid_array, Y_truth_grid_array] = np.meshgrid(x_truth_span_array, y_truth_span_array)

