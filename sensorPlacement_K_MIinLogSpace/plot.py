# -*- coding: utf-8 -*-
"""
Created on Wed Sep 30 22:38:02 2015

@author: xiaolin
"""

import pickle
import matplotlib.pyplot as plt
from mainsetting import *

with open('postDistribution.pickle') as p:
    mean_Post_entropy, updated_samples_list, best_design_list_list = pickle.load( p)

if num_trials == 1:
    samples = deepcopy(updated_samples_list[0])
else:
    samples = deepcopy(updated_samples_list)  
    
    
# plot entropy
plt.figure()
plt.plot(range(1, num_stages + 1), mean_Post_entropy,'o-')
plt.xticks(range(1, num_stages + 1))
plt.xlabel("Updates")
plt.ylabel("Entropy")
plt.savefig('Figures/Entropy.eps',format='eps', dpi=1200)
plt.savefig('Figures/Entropy.pdf',format='pdf', dpi=1200)

# plot update samples
plt.figure()
plt.subplot(2,2,1)
plt.hist(updated_samples_list[0],100)
plt.axis([0.5, 2.0, 0, 80])
#plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'y')
plt.ylabel("Number of samples")
plt.locator_params(nbins=4)

plt.title( ' the 1st update')

plt.subplot(2,2,2)
plt.hist(updated_samples_list[1],100)
plt.axis([0.5, 2.0, 0, 80])
#plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'y')
plt.locator_params(nbins=4)

plt.title( ' the 2nd update')

plt.subplot(2,2,3)
plt.hist(updated_samples_list[2],100)
plt.axis([0.5, 2.0, 0, 80])
#plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'y')
plt.title( ' the 3rd update')
plt.xlabel("K")
plt.ylabel("Number of samples")
plt.locator_params(nbins=4)


plt.subplot(2,2,4)
plt.hist(updated_samples_list[3],100)
plt.axis([0.5, 2.0, 0, 80])
#plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'y')
plt.title( ' the 4th update')
plt.xlabel("K")
plt.locator_params(nbins=4)


plt.tight_layout() 

plt.savefig('Figures/postSamples.eps',format='eps', dpi=1200)
plt.savefig('Figures/postSamples.pdf',format='pdf', dpi=1200)