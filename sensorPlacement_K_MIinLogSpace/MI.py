
from modelfunction import *
from utils import *
import numpy as np
import pickle
from copy import deepcopy

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt



with open('p_pf_initial.pickle') as f:
    p_pf_prop = pickle.load(f)
from mainsetting import *



MI_grid_array = deepcopy(X_truth_grid_array)

num_designs = X_truth_grid_array.size


for i in range(0,X_truth_grid_array.shape[0]):
    for j in range(0,X_truth_grid_array.shape[1]):
        addnew = [X_truth_grid_array[i][j], Y_truth_grid_array[i][j]]
        MI_grid_array[i][j] = funcMI(addnew, p_pf_prop, num_samples, meas_std)

        
fig = plt.figure()
ax = fig.gca(projection='3d')
surf = ax.plot_surface(X_truth_grid_array, Y_truth_grid_array, MI_grid_array, rstride=1, cstride=1, cmap=cm.coolwarm,
        linewidth=0, antialiased=False)
ax.zaxis.set_major_locator(LinearLocator(3))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

fig.colorbar(surf, shrink=0.5, aspect=5)
ax.set_xlabel('x')
ax.set_xlim(1, 5)
ax.set_xticks([1, 3, 5])
ax.set_ylabel('y')
ax.set_ylim(-2, 2)
ax.set_yticks([-2, 0, 2])
ax.set_zlabel('z')

#ax.view_init(elev=elevation_angle, azim=azimuthal_angle)
#plt.savefig('destination_path_MI.eps', format='eps', dpi=300)
plt.savefig('Figures/3dMI.eps',format='eps', dpi=1200)


plt.figure()
plt.pcolor(X_truth_grid_array, Y_truth_grid_array, MI_grid_array, cmap='rainbow', vmin=MI_grid_array.min(), vmax=MI_grid_array.max())
plt.title('mutual information map')
# set the limits of the plot to the limits of the data
plt.axis([X_truth_grid_array.min(), X_truth_grid_array.max(), Y_truth_grid_array.min(), Y_truth_grid_array.max()])
plt.colorbar()
plt.xticks([1,3,5])
plt.yticks([-2,0,2])
plt.ylabel('y')
plt.xlabel('x')
plt.savefig('Figures/2dMI.eps',format='eps', dpi=1200)



