import numpy as np
from modelfunction import *
import pystan
from copy import deepcopy

from BayesO import *

#---------------------------------------------------------------------------    
# entropy approximation
#---------------------------------------------------------------------------
def knnENT_GaussApprox(my_chain, my_d):
        
#--------------------------------------------------------------------------
# Normalize the chain
#--------------------------------------------------------------------------
    my_Sigma = np.cov(my_chain.T)
#--------------------------------------------------------------------------
# Estimate mutual information
#--------------------------------------------------------------------------
   # (sign, logdet) = np.linalg.slogdet(my_Sigma) 
    if my_d == 1:
        exactENT = np.log( np.sqrt( (2*np.pi*np.exp(1))**my_d  * my_Sigma ))
    else:   
        exactENT = np.log( np.sqrt( (2*np.pi*np.exp(1))**my_d  * np.linalg.det(my_Sigma) ))
        
    ent = exactENT
    return ent
'''
#---------------------------------------------------------------------------
# KL divergence approximation    
#---------------------------------------------------------------------------
def knnKL_GaussApprox(prior, poster, dim):

    mean_post = np.mean( poster.T,axis=0)
    mean_prior = np.mean( prior.T,axis=0)
    Sigma_post = np.cov( poster)
    Sigma_prior = np.cov( prior)
        
    inv_Sigma_prior = 1/Sigma_prior
    det_Sigma_prior = Sigma_prior
    det_Sigma_post = Sigma_post
    kl = 1/2*( np.dot(inv_Sigma_prior,Sigma_post) +np.dot( np.dot((mean_prior-mean_post),inv_Sigma_prior),(mean_prior-mean_post).T) - np.log( det_Sigma_post/det_Sigma_prior ) - dim )

    return kl
'''
#---------------------------------------------------------------------------
# mutual information approximation    
#---------------------------------------------------------------------------    
def knnMI_GaussApprox(my_chain,my_split):

    [lenChain, dimChain] = my_chain.shape
    my_dx = my_split[0]
    my_dy = my_split[1]
    my_d = sum(my_split)

    if( my_d != dimChain ):
        print('ERROR: the joint dimension in the split is different from the dimension of the chain')
        import sys
        sys.exit(1)

#--------------------------------------------------------------------------
# Normalize the chain
#--------------------------------------------------------------------------
    my_Sigma = np.cov(my_chain.T)
#--------------------------------------------------------------------------
# Estimate mutual information
#--------------------------------------------------------------------------
    my_Sigma_x = my_Sigma[0:my_dx][:,0:my_dx]
    my_Sigma_y = my_Sigma[my_dx:][:,my_dx:]
    (sign_x, logdet_x) = np.linalg.slogdet(my_Sigma_x) 
    (sign_y, logdet_y) = np.linalg.slogdet(my_Sigma_y) 
    (sign, logdet) = np.linalg.slogdet(my_Sigma) 
    exactENT_x = np.log( np.sqrt( (2*np.pi*np.exp(1))**my_dx  ) ) + logdet_x
    exactENT_y = np.log( np.sqrt( (2*np.pi*np.exp(1))**my_dy ) ) + logdet_y
    exactENT_xy = np.log( np.sqrt( (2*np.pi*np.exp(1))**my_d  ) ) + logdet

    exactMI = exactENT_x + exactENT_y - exactENT_xy

    mi = exactMI
    return mi

 
#--------------------------------------------------------------------------
# observation with additive noise  dim = 2 means n by 2, 1means row vector, o means scaler
#--------------------------------------------------------------------------    
def simulateMeasAdd( cur_state, meas_std, dim): 
    
    if (dim == 2):
        err_norm = np.random.normal(0, 1, (cur_state.shape[0], cur_state.shape[1]))
    elif (dim == 1):
        err_norm = np.random.normal(0, 1, (cur_state.shape[0]))
    elif (dim ==0 ):
        err_norm = np.random.normal(0 ,1)
    else:
        print "the state variabe has wrong dimention !"

    
    sim_meas = cur_state + meas_std * err_norm
    
    return sim_meas
    

 
#--------------------------------------------------------------------------
# observation with multiplicative noise dim = 2 means n by 2, 1means row vector, o means scaler
#--------------------------------------------------------------------------    
def simulateMeasMult( cur_state, logmu, logstd, dim ):
    
    if (dim == 2):
        err_norm = np.random.normal(logmu, logstd, (cur_state.shape[0], cur_state.shape[1]))
    elif (dim == 1):
        err_norm = np.random.normal(logmu, logstd, (cur_state.shape[0]))
    elif (dim ==0 ):
        err_norm = np.random.normal(logmu, logstd)
    else:
        print "the state variabe has wrong dimention !"    

    #err_norm = np.random.normal(logmu, logstd, (cur_state.shape[0], cur_state.shape[1]))
    
    
    log_sim_meas = np.log(cur_state + 1e-100) + err_norm
    sim_meas = np.exp(log_sim_meas)
    
    
    return sim_meas
          

#--------------------------------------------------------------------------
# pystan solution for inverse problem
#--------------------------------------------------------------------------    
def stan_MCMCupdateGP(x_sofar, obs_sofar, no_smps, R):

    vec_data1 = []
    vec_data2 = []

    for i in range(0, len(x_sofar)):
        vec_data1.append(x_sofar[i][0])
        vec_data2.append(x_sofar[i][1])


    input_x1 = vec_data1
    input_x2 = vec_data2
    output_y = obs_sofar


    Branin_dat = {'N':len(x_sofar),'x':input_x1,'y':input_x2,'d':output_y,'R':R}
    first_Branin_dat = {'N':len(x_sofar),'x':x_sofar[0][0],'y':x_sofar[0][1],'d':output_y[0],'R':R}

    if len(x_sofar)==1:
        fit1 = pystan.stan(file = 'first_diffusion.stan', data = first_Branin_dat, iter = no_smps, chains = 2, seed = 1)
        la1 = fit1.extract(permuted=True)  
        
        theta = la1['theta']

    else: 
        fit2 = pystan.stan(file = 'diffusionModel.stan', data = Branin_dat, iter = no_smps, chains = 2, seed = 1);
        la2 = fit2.extract(permuted=True) 
        
        theta = la2['theta']
        
    samples = theta
   
    print theta.shape

    posterior = samples
    
    return posterior


#---------------------------------------------------------------------------
# funcMI
#---------------------------------------------------------------------------

def funcMI(design, samples, num_samples, meas_std):
    
    my_state = np.zeros(num_samples)
    my_chain_data = np.zeros( num_samples)
    
    for i in range(0, num_samples):	              
        my_state[i] = SimFunc(design[0], design[1], samples[i])
        my_chain_data[i] = simulateMeasAdd( my_state[i], meas_std, 0 )
    
    my_chain = np.vstack((my_chain_data,samples )).T                 		
    MI_est = knnMI_GaussApprox( my_chain,[1,1]) 
            
    return MI_est
    
    
#---------------------------------------------------------------------------
# Bayesian optimazition
#---------------------------------------------------------------------------
    
def BayesOpt_MI(domain, samples, num_samples, meas_std): 
    
    def funcMI(x , y):
        def funcMI_inner(x, y, samples, num_samples, meas_std):
            my_state = np.zeros(num_samples)
            my_chain_data = np.zeros( num_samples)
    
            for i in range(0, num_samples):	              
                my_state[i] = SimFunc(x, y, samples[i])
                my_chain_data[i] = simulateMeasAdd( my_state[i], meas_std, 0 )
    
            my_chain = np.vstack((my_chain_data,samples )).T                 		
            MI_est = knnMI_GaussApprox( my_chain,[1,1]) 
            
            return MI_est
        return funcMI_inner(x, y, samples, num_samples, meas_std)
    
    bo = BayesianOptimization(funcMI, {'x': (domain[0][0], domain[0][1]), 'y':(domain[1][0], domain[1][1]) })
    bo.explore({'x': [3], 'y': [2]})
    bo.maximize(n_iter=15)
    print(bo.res['max'])
    print bo.res['max']['max_params']
    
    return [bo.res['max']['max_params']['x'], bo.res['max']['max_params']['y']]
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    