
'''
import sys
sys.path.append("utils")


'''

from modelfunction import *
from utils import *
import numpy as np
import pickle
from copy import deepcopy

# ------------------------------------------------------------------------
# Strategy number
#--------------------------------------------------------------------------

no_strategy = 1

# ------------------------------------------------------------------------
# Load measurements
# Get global data by running "mainsetting.m"
#--------------------------------------------------------------------------

with open('p_pf_initial.pickle') as f:
    p_pf_prop = pickle.load(f)
from mainsetting import *

# ------------------------------------------------------------------------
# Main Simulation
#--------------------------------------------------------------------------

trials_Post_entropy = np.zeros((num_trials, num_stages))
#trials_KL = np.zeros((num_trials, num_stages))
trials_MI_est = np.zeros((num_trials, num_stages))

#test_list = [[1.,-1.],[3.,0.],[4.,0.5]]

updated_samples_list = [0]*num_trials
updated_samples = [0]*num_stages
best_design_list_list = [0]*num_trials

for trial in range(0, num_trials):
    print( '   |- Process trial %d / %d\n' % (trial, num_trials))

    best_design_list = []
    obs_best_design_list = []

    for k_stage in range(0 , num_stages):	    
        print( '   |------ Process stage %d / %d\n' % (k_stage, num_stages))        
        
        best_design = BayesOpt_MI(deepcopy(domain), deepcopy(p_pf_prop), num_samples, meas_std)
        
        best_design_list.append(deepcopy(best_design) )       
        truth_best_design = SimFunc(best_design[0], best_design[1], K_truth)
        obs_best_design = simulateMeasAdd(truth_best_design, meas_std,0) 
        obs_best_design_list.append(deepcopy(obs_best_design))
        
        p_pf_update = stan_MCMCupdateGP (best_design_list, obs_best_design_list, num_samples,R)
        
        updated_samples[k_stage] = deepcopy(p_pf_update)
             	
        trials_Post_entropy[trial, k_stage] = knnENT_GaussApprox( p_pf_update.T, num_para)
        #trials_KL[trial, k_stage] = knnKL_GaussApprox(p_pf_prop, p_pf_update, 1)
		  
        p_pf_prop = deepcopy(p_pf_update)
    updated_samples_list[trial] = deepcopy(updated_samples )
    best_design_list_list[trial] = deepcopy(best_design_list )    
    


if num_trials > 1:   
    mean_Post_entropy = np.mean(trials_Post_entropy,axis = 0)
    #mean_KL = np.mean(trials_KL,axis = 0)

else:
    mean_Post_entropy = trials_Post_entropy[0] 
    updated_samples_list = updated_samples_list[0]
    best_design_list_list = best_design_list_list[0] 
    # first sensor location: [5.0, -0.0005382749264478378]
   # mean_KL = trials_KL

with open('postDistribution.pickle', 'w') as p:
    pickle.dump([mean_Post_entropy, updated_samples_list, best_design_list_list], p)
