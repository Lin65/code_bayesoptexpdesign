'''
Created on Oct 21, 2014

@author: xiaolin
'''

import numpy as np

def RealFunc(X, Y):
    
    H = 1
    u = 1
    Q = 10
    frac1 = 6*Q
    frac2 = np.pi*u*X**2
    f1 = np.log(frac1/frac2)
    frac3 = 3*u*(Y**2+H**2)
    frac4 = u*X**2
    f2 = -frac3/frac4
    c = f1 + f2

   # Z = (Y- 5.1*(X/(2*np.pi))**2 + 5*X/np.pi -6)**2 + 10*(1 - 1/(8*np.pi))*np.cos(X) + 10   

    return c
    
def SimFunc( X, Y, K ):
    H = 1
    u = 1
    Q = 10
    frac1 = 2*np.pi*K*X
    f1 = np.log(Q/frac1)
    frac2 = u*(Y**2+H**2)
    frac3 = 4*K*X
    f2 = -frac2/frac3
    c = f1 + f2
 
    
    return c